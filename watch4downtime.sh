#!/bin/bash

TARGET="$1"
DELAY=2

while ping -c 1 $TARGET -W 5 >/dev/null; do
	echo $(date -Isec) : $TARGET is alive
	sleep $DELAY
done

while ! ping -c 1 $TARGET -W 5 >/dev/null; do
	echo $(date -Isec) : $TARGET is not responding
	sleep $DELAY
done

echo $(date -Isec) : $TARGET is alive
