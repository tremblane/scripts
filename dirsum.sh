#!/bin/bash

#check given dir, or cwd if not given
targetDir=${1:-"."}

cd "$targetDir"

{ export LC_ALL=C
find . -name '.snapshot' -prune -o -type f -exec sha256sum {} + | sort
echo
find . -name '.snapshot' -prune -o -type d -print | sort | sha256sum; } | sha256sum
