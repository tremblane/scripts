#!/bin/bash

usage() {
	echo "Usage: $0 -d <drive letter>"
}

while getopts "d:uh" OPT; do
	case $OPT in
		d)
			drive_letter="${OPTARG}"
			;;
		u)
			unmount="true"
			;;
		h)
			usage
			exit 0
			;;
		*)
			echo "Unknown option: $OPT"
			echo
			usage
			exit 1
			;;
	esac
done

if [[ -z "$drive_letter" ]]; then
	echo "Missing required option: -d <drive_letter>"
	echo
	usage
	exit 1
fi

if [[ ! "$drive_letter" =~ ^[a-z]$ ]]; then
	echo "ERROR: Drive letter must be single character, lower case"
	exit 1
fi

mount_base="/mnt"
mount_path=${mount_base}/${drive_letter}

if [[ "$unmount" == "true" ]]; then
	echo sudo umount $mount_path
	sudo umount $mount_path
else
	echo sudo mkdir -p $mount_path
	sudo mkdir -p $mount_path
	echo sudo mount -t drvfs ${drive_letter}: $mount_path
	sudo mount -t drvfs ${drive_letter}: $mount_path
fi
