#!/bin/bash

target="$1"

if [[ -z "$target" ]]; then
	echo "Usage $(basename $0) <task ID>"
	exit 1
fi

task_desc="$(task ${target} export | jq -r '.[].description')"
if [[ -z "$task_desc" ]]; then
	echo "no description found for task $target"
	exit 1
fi
task_tags="$(task +reminder $target export | jq -r '.[].tags | .[]' 2>/dev/null | tr '\n' ' ')"

# Prompt to continue
echo "Task: ${task_desc}"
if [[ ! -z "$task_tags" ]]; then
	echo "Tags: $task_tags"
fi
echo
read -e -N 1 -p "Pend/complete this task? [y/N] " choice
if [[ "$choice" != [Yy] ]]; then
	exit 1
fi

if task +reminder +daily $target information >/dev/null 2>&1; then
	echo "daily task"
	echo "task $target duplicate wait:tomorrow"
	task $target duplicate wait:tomorrow
	task $target rc.confirmation=0 rm
elif task +reminder +weekly $target information >/dev/null 2>&1; then
	echo "weekly task"
	echo "task $target duplicate wait:$(date +%A)"
	task $target duplicate wait:$(date +%A)
	task $target rc.confirmation=0 rm
else
	recur_day="$(task +reminder $target export | jq -r '.[].tags | .[]' | grep ^after | sed 's/after//')"
	if [[ -z "$recur_day" ]]; then
		echo "not recurring"
		task $target done
	else
		echo "repeat after ${recur_day}"
		echo "task $target duplicate wait:${recur_day}"
		task $target duplicate wait:${recur_day} && task rc.confirmation=0 $target delete
	fi
fi
