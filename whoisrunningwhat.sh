#!/bin/bash

for X in $(who | awk '{print $1}' | sort -u); do ps -f fwwp $(pgrep -u $X); done | sed '2,${;/^UID/d;}'
