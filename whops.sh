#!/bin/bash

#alias whops="ps -f fwwp \$(for user in \$(who | awk '{print \$1}' | grep -v root | sort); do pgrep -u \$user; done; for tty in \$(who | awk '{print \$2}' | sort -n); do pgrep -t \$tty; done)"

ps -f fwwp $(for user in $(who | awk '{print $1}' | grep -v root | sort); do pgrep -u $user; done; for tty in $(who | awk '{print $2}' | sort -n); do pgrep -t $tty; done)
