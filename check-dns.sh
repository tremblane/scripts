#!/bin/bash

if [[ -z "$1" ]]; then
	echo "Usage: $(basename "$0") [hostname or IP]"
	echo
	exit 1
fi

input="$1"


initialresult=$(getent ahostsv4 "${input}")
if [[ -z "${initialresult}" ]]; then
	echo "FAIL - no result for ${input}"
	exit 1
fi
fullinput=$(echo "${initialresult}" | head -n 1 | awk '{print $NF}')
result1=$(getent ahostsv4 "${input}" | awk '{print $1}' | head -n 1)

if [[ "$fullinput" = "$result1" ]]; then
	result1=$(getent hosts "${input}" | awk '{print $NF}' | head -n 1)
	if [[ -z "${result1}" ]]; then
		echo "FAIL - no result for ${input}"
		exit 1
	fi
	result2=$(getent ahostsv4 "${result1}" | awk '{print $1}' | head -n 1)
else
	result2=$(getent hosts "${result1}" | awk '{print $NF}' | head -n 1)
fi

if [[ -z "${result2}" ]]; then
	echo "FAIL - ${fullinput} -> ${result1}, no result for ${result1}"
	exit 1
fi

if [[ "${fullinput}" = "${result2}" ]]; then
	echo "OK - ${fullinput} -> ${result1} -> ${result2}"
	exit 0
else
	echo "FAIL - ${fullinput} -> ${result1} -> ${result2}"
	exit 1
fi
