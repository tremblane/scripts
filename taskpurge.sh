#!/bin/bash

completed_months=6
purge_months=12

# Delete old completed tasks
## list before deleting
echo == Listing tasks completed over ${completed_months} months ago
task end.before:today-${completed_months}mo completed
echo == Deleting tasks completed over ${completed_months} months ago
task end.before:today-${completed_months}mo rm

# Purge old deleted tasks
echo == Purging deleted tasks completed over ${purge_months} months ago
task end.before:today-${purge_months}mo purge
