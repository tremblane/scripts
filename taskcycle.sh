#!/bin/bash

target="$1"

if [[ -z "$target" ]]; then
	echo "ERROR: no task given"
	exit 1
fi

target_string="$2"
if [[ -z "$target_string" ]]; then
	target_string="cycle"
fi

if ! task $target information >/dev/null ; then
	exit 1
fi

annotation_string="$(task $target denotate $target_string | grep ^Found | awk -F\' '{print $2}')"
if [[ -z "$annotation_string" ]]; then
	echo "No matching annotation"
else
	echo "Cycling annotation: " $annotation_string
	task $target annotate -- $annotation_string
fi
