#!/bin/bash

# Must have at least one PID
if [[ -z "$1" ]]; then
	echo "Usage: $0 PID [PID...]"
	exit 1
fi
# You want PID 1? Really? Ok...sure. Let's do that here and not mess with my loop conditions...
if [[ "$1" == "1" ]]; then
	ps -f fwwp 1
	exit
fi

pid_list=""

#loop over every parameter passed in
for new_pid in $*; do
	# Make sure new_pid is at least look like an integer
	if ! [ "$new_pid" -eq  "$new_pid" ] 2>/dev/null; then
		echo "skipping $new_pid, does not appear to be an integer" >&2
		continue
	fi
	# Get the ancestry of each pid
	while [[ "$new_pid" != "1" ]]; do
		#append the pid to the list
		pid_list="$pid_list $new_pid"
		#get the output of ps for the pid
		ps_output="$(ps -f fwwp $new_pid)"
		#pull the ppid from the ps output
		ppid="$(echo "$ps_output" |  tail -n +2 | head -n 1 | awk '{print $3}')"
		#if ppid ends up blank, e.g. no process with the pid, break out of the loop
		if [[ -z "$ppid" ]]; then
			break
		fi
		#shuffle the ppid to be the new pid to look for
		new_pid=$ppid
	done
done

# We're done, print out the listing for every pid found, printed in a pretty format
if [[ ! -z "$pid_list" ]]; then
	ps -f fwwp $pid_list
else
	echo "No process ids found" >&2
fi
