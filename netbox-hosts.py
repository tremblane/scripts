#!/usr/bin/python

import argparse
import sys
import requests
import urllib3
import yaml
import json

def log_stderr(message):
    sys.stderr.write(message+"\n")
    sys.stderr.flush()


def get_api_response(url,auth_data,start="0"):
    urllib3.disable_warnings()
    headers = {
                "Authorization": "Token " + auth_data['auth_token'],
                "Accept": "application/json"
            }
    payload = {
            "status": "active"
            }
    results = []
    while url is not None:
        response = requests.get(url,headers=headers,params=payload,verify=False)
        response_json=response.json()
        url = response_json["next"]
        results.extend(response_json["results"])
    return results


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--server', help='Netbox URL', required=True)
    parser.add_argument('-t','--tokenfile', help='token file', required=True)
    parser.add_argument('-a','--all', help='all host details', required=False, action='store_true')
    args = parser.parse_args()

    token_file = args.tokenfile
    netbox_server = args.server

    with open(token_file,"r") as f:
        token = str(f.read())
    auth_token = token.strip("\n")

    auth_data = {'auth_token': auth_token}
    base_url = "https://" + args.server + "/api"

    # Get hosts from Netbox
    all_hosts = []
    host_endpoints = [ "/dcim/devices", "/virtualization/virtual-machines" ]
    for host_endpoint in host_endpoints:
        hosts = get_api_response(base_url + host_endpoint, auth_data)
        if hosts is not None:
            all_hosts.extend(hosts)

    # print host details
    if args.all is True:
        print(json.dumps(all_hosts))
    else:
        for host in all_hosts:
            print(host["name"])

if __name__ == '__main__':
    main()
